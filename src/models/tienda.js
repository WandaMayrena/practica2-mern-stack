const mongoose = require('mongoose');
const { Schema } = mongoose;

const Cliente = new Schema({
	nombre_c: { type: String, required: true },
	cedula_c: { type: String, required: true},
	edad_c: { type: Number, required: true},
	genero_c: { type: String, required: true },
	fecha_nac_c: { type: String, required: true},
	direccion_c: { type: String, required: true}
}); 



const Producto = new Schema({
	nombre: { type: String, required: true },
	cantidad: { type: String, required: true},
	//categoria: { type: String, enum:
	//	['embutidos', 'vegetales', 'frutas', 'carnes'] },
	precio: { type: Number, required: true }
}); 

/*
const Personal_adm = new Schema({
	Nombre: { type: String, required: true },
	Cedula: { type: String, required: true},
	edad: { type: String, required: true},
	genero: { type: String, required: true },
	fecha_nac: { type: String, required: true},
	Direccion: { type: String, required: true},
	Usuario: { type: String, required: true },
	contraseña: { type: String, required: true},
	Email: { type: String, required: true}
}); 
*/

module.exports = mongoose.model('Cliente', Cliente);
module.exports = mongoose.model('Producto', Producto);
//module.exports = mongoose.model('Personal_adm', Personal_adm);
