const mongoose = require('mongoose');
const { Schema } = mongoose;

const Personal_adm = new Schema({
	nombre_pA: { type: String, required: true },
	cedula_pA: { type: String, required: true},
	edad_pA: { type: Number, required: true},
	genero_pA: { type: String, required: true },
	fecha_nac_pA: { type: String, required: true},
	direccion_pA: { type: String, required: true},
	usuario: { type: String, required: true },
	contraseña: { type: String, required: true},
	email: { type: String, required: true}
}); 


module.exports = mongoose.model('Personal_adm', Personal_adm);

