const mongoose = require('mongoose');
const { Schema } = mongoose;

const Cliente = new Schema({
	nombre_c: { type: String, required: true },
	cedula_c: { type: String, required: true},
	edad_c: { type: Number, required: true},
	genero_c: { type: String, required: true },
	fecha_nac_c: { type: String, required: true},
	direccion_c: { type: String, required: true}
}); 

module.exports = mongoose.model('Cliente', Cliente);
