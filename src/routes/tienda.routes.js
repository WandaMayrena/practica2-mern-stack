const express = require('express');
const router = express.Router();

//const Cliente = require('../models/cliente');
//const Producto = require('../models/producto');
const PersAdm = require('../models/persAdm');

/*
// Obtener las peticiones:
router.get('/', async (req, res) => {
	const clientes = await Cliente.find();
	res.json(clientes); 
});
*/
/*
router.get('/', async (req, res) => {
	const productos = await Producto.find();
	res.json(productos); 
});
*/

router.get('/', async (req, res) => {
	const persAdm = await PersAdm.find();
	res.json(persAdm); 
});

/*
// Obtener un unico cliente
router.get('/:id', async (req, res) => {
	const cliente = await Cliente.findById(req.params.id);
	res.json(cliente);
});
*/

/*
// Obtener un unico Producto
router.get('/:id', async (req, res) => {
	const producto = await Producto.findById(req.params.id);
	res.json(producto);
});
*/

// Obtener un unico de personal
router.get('/:id', async (req, res) => {
	const persAdm = await PersAdm.findById(req.params.id);
	res.json(persAdm);
});


// enviar datos a la BD:
// almacenar en la base de datos
/*
router.post('/', async (req, res) => {
	const { nombre_c, cedula_c, edad_c, genero_c, fecha_nac_c, direccion_c } = req.body;
	const cliente = new Cliente({nombre_c, cedula_c, edad_c, genero_c, fecha_nac_c, direccion_c});
	await cliente.save();

	res.json({status: 'Client Saved'});
}); 
*/
/*
router.post('/', async (req, res) => {
	const { nombre, cantidad, categoria, precio } = req.body;
	const producto = new Producto({nombre, cantidad, categoria, precio});
	await producto.save();

	res.json({status: 'Product Saved'});
});
*/ 

router.post('/', async (req, res) => {
	const { nombre_pA, cedula_pA, edad_pA, genero_pA, fecha_nac_pA, direccion_pA, usuario, contraseña, email } = req.body;
	const personal = new PersAdm({nombre_pA, cedula_pA, edad_pA, genero_pA, fecha_nac_pA, direccion_pA, usuario, contraseña, email});
	await personal.save();

	res.json({status: 'Persona adm Saved'});
}); 

/*
// actualizar Cliente
router.put('/:id', async (req, res) => {
	const { nombre_c, cedula_c, edad_c, genero_c, fecha_nac_c, direccion_c } = req.body;
	const newClient = {nombre_c, cedula_c, edad_c, genero_c, fecha_nac_c, direccion_c};
	await Cliente.findByIdAndUpdate(req.params.id, newClient);
	res.json({status: 'Client Updated!'});
});
*/

/*
// actualizar Producto
router.put('/:id', async (req, res) => {
	const { nombre, cantidad, categoria, precio} = req.body;
	const newProduct = {nombre, cantidad, categoria, precio};
	await Producto.findByIdAndUpdate(req.params.id, newProduct);
	res.json({status: 'Person Updated!'});
});
*/

// actualizar Personal Adm
router.put('/:id', async (req, res) => {
	const { nombre_pA, cedula_pA, edad_pA, genero_pA, fecha_nac_pA, direccion_pA, usuario, contraseña, email } = req.body;
	const newPers = {nombre_pA, cedula_pA, edad_pA, genero_pA, fecha_nac_pA, direccion_pA, usuario, contraseña, email};
	await PersAdm.findByIdAndUpdate(req.params.id, newPers);
	res.json({status: 'Person Updated!'});
});

/*
// Eliminar Cliente
router.delete('/:id', async (req, res) => {
	await Cliente.findByIdAndRemove(req.params.id);
	res.json({status: 'Client Deleted'});
});
*/

/*
// Eliminar Producto
router.delete('/:id', async (req, res) => {
	await Producto.findByIdAndRemove(req.params.id);
	res.json({status: 'Product Deleted'});
});
*/

// Eliminar Personal
router.delete('/:id', async (req, res) => {
	await PersAdm.findByIdAndRemove(req.params.id);
	res.json({status: 'Person Deleted'});
});

module.exports = router;